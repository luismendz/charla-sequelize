const Sequelize = require('sequelize');

const sequelize = new Sequelize(
    'Libreria',
    'test',
    'Test1234',
    {
        host: 'localhost',
        port: 5432,
        dialect: 'postgres',
        logging: ((...msg) => console.log(msg)),
        schema: 'exercieses',
        define: {
            underscored: true,
            freezeTableName: false,
            paranoid: false,
            timezone: 'America/Santiago'
        }
    }
);

module.exports = sequelize;