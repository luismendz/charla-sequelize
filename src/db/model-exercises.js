const sequelize = require('./index-exercises');
const {Sequelize, Model, DataTypes} = require('sequelize');

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});

// Crear los modelos descritos a continuación en orden de probar los diferentes

// Crear un modelo con un atributo dejando toda la nomenclatura por defecto. (Revisar resultado en base de datos.)
const Test = sequelize.define('testModel', {attr1: {type: DataTypes.STRING}}, {underscored: false, freezeTableName: false});
/* Recuerda que ya algunas opciones veinen preconfiguradas desde la definición de la instancia 'sequelize' para los modelos. */

// Crear un modelo con al menos 2 atributo, con sus nombres autogenerados escribiendolos en camelCase. Y con la opción de underscored habilitada.

// Crear un modelo con al menos 2 atributo, uno con su nombre autogenerado y otro con el nombre forzado. Y con la opción de underscored habilitada.

// Crear un modelo con al menos 1 atributo, con el  nombre del modelo en camelCase, la opción underscored activa y la opción freezeTableName activa.

// Crear modelo con un atributo, definiendo exactamente los nombres de tabla y columna diferentes a los del modelo y atributo y sin que se creen las columnas de fecha de creación ni fecha de edición.

// Crear un modelo con al menos un atributo y con la opción de paranoia activada.

// Crear un modelo con al menos un atributo en el esquema 'public'.

// Revisar cada una de las tablas y atributos generadas en DB a aprtir de este ejercicio, y eliminarla luego para no dejar datos basura en DB que luego puedan generar problemas con otros ejercicios.


readline.question(`Are you sure you want to sycn the models? All data in the affected tables will be reseted. (Y/N)`, resp => {
    if (['y', 'yes', 'si', 's', 'ok'].includes(resp.toLowerCase())) {
        (async () => {
            try {
                await sequelize.sync({force: true});
            } catch (err) {
                console.log('Error inicializando la Data');
                console.error(err);
            } finally {
                end();
            }
        })();
    }
    readline.close();
});
const end = async () => {
    await sequelize.close();
    process.exit()
}
