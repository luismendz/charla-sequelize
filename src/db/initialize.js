const sequelize = require('./index');
const fs = require('fs');
const Genre = require('./models/genre');
const genreSQL = fs.readFileSync(__dirname + '/data/genres.sql').toString();
const Book = require('./models/book');
const booksSQL = fs.readFileSync(__dirname + '/data/books.sql').toString();
const Author = require('./models/author');
const authorsSQL = fs.readFileSync(__dirname + '/data/authors.sql').toString();
const Classification = require('./models/classification');
const classificationsSQL = fs.readFileSync(__dirname + '/data/classifications.sql').toString();
const Client = require('./models/client');
const clientsSQL = fs.readFileSync(__dirname + '/data/clients.sql').toString();
const create_salesSQL = fs.readFileSync(__dirname + '/create-sales.sql').toString();
const salesSQL = fs.readFileSync(__dirname + '/data/sales.sql').toString();
// const Sale = require('./models/sale');

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});

readline.question(`Are you sure you want to sycn the models? All data in the affected tables will be reseted. (Y/N)`, resp => {
    if (['y', 'yes', 'si', 's', 'ok'].includes(resp.toLowerCase())) {
        (async () => {
            try {
                await sequelize.sync({force: true});
                await loadData();
                console.log('Inicialización OK');
                /* const e = await Author.findOne({
                    include: {
                      model: Book.scope('family_book')
                    }
                  });
                console.log(e.get({plain: true})); */
            } catch (err) {
                console.log('Error inicializando la Data');
                console.error(err);
            } finally {
                end();
            }
        })();
    }
    readline.close();
});
const end = async () => {
    await sequelize.close();
    process.exit()
}
const loadData = async () => {
    await sequelize.query(genreSQL);
    await sequelize.query(authorsSQL);
    await sequelize.query(classificationsSQL);
    await sequelize.query(booksSQL);
    await sequelize.query(clientsSQL);
    await sequelize.query(create_salesSQL);
    await sequelize.query(salesSQL);
}
