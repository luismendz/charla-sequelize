INSERT INTO charla.classifications ("name","label",description,reference_age,licence_required,created_at,updated_at) VALUES
	('A','Clase A','Todo público',0,false,current_timestamp,current_timestamp),
	('B','Clase B','Adolescentes',13,false,current_timestamp,current_timestamp),
	('C','Clase C','Adulto Joven',18,false,current_timestamp,current_timestamp),
	('D','Clase D','Adulto',21,false,current_timestamp,current_timestamp),
	('Extra','Clase Extra','Requiere licencia',21,true,current_timestamp,current_timestamp);
