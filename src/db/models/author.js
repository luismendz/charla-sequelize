const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../index');

const Author = sequelize.define('author',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        birthday: {
            type: DataTypes.DATEONLY,
            allowNull: true,
        }
    },
    {
        // Otras Opciones
    }
);

module.exports = Author;