const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../index');

const Classification = sequelize.define('classification',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        label: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        reference_age: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        licence_required: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
    },
    {
        // Otras Opciones
    }
);


module.exports = Classification;