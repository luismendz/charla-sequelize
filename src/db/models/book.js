const { Sequelize, DataTypes, Model, Op } = require('sequelize');
const sequelize = require('../index');
const Author = require('./author');
const Classification = require('./classification');
const Genre = require('./genre');

class Book extends Model {}
Book.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        active: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
        author_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: Author,
                key: 'id',
            }
        },
        classification_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: Classification,
                key: 'id',
            }
        },
        genre_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: Genre,
                key: 'id',
            }
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false,
        },
        year: {
            type: DataTypes.INTEGER,
            allowNull: true,
        }
    },
    {
        sequelize, // Instancia de Sequelize a la que pertenece el modelo.
        modelName: 'book',
        defaultScope: {
            where: {
              active: true
            }
        },
        scopes: {
            family_book: {
                include: {
                    model: Classification,
                    where: {
                        reference_age: {[Op.lt]: 18},
                        active: true,
                        licence_required: false
                    }
                }
            }
        }
    }
);

Book.addScope('mystery_book', {
    where: {
        genre_id: {[Op.in]: Sequelize.literal(`(select "g"."id" from "charla"."genres" "g" where "g"."name" = 'mystery')`)}
    }
});

Book.belongsTo(Genre, {foreignKey: { name: 'genre_id', type: DataTypes.INTEGER, allowNull: true}, sourceKey: 'id'});
Genre.hasMany(Book, {foreignKey: { name: 'genre_id', type: DataTypes.INTEGER, allowNull: true}, sourceKey: 'id'});

Book.belongsTo(Author, {foreignKey: { name: 'author_id', type: DataTypes.INTEGER, allowNull: true}, sourceKey: 'id'});
Author.hasMany(Book, {foreignKey: { name: 'author_id', type: DataTypes.INTEGER, allowNull: true}, sourceKey: 'id'});

Book.belongsTo(Classification, {foreignKey: { name: 'classification_id', type: DataTypes.INTEGER, allowNull: true}, sourceKey: 'id'});
Classification.hasMany(Book, {foreignKey: { name: 'classification_id', type: DataTypes.INTEGER, allowNull: true}, sourceKey: 'id'});

module.exports = Book;