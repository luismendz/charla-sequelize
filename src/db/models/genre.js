const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../index');
const Author = require('./author');

const Genre = sequelize.define('genre',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        label: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    },
    {
        timestamps: null
    }
);

Author.belongsToMany(Genre, {through: 'author_genres', sourceKey: 'id'});
Genre.belongsToMany(Author, {through: 'author_genres', sourceKey: 'id'});

module.exports = Genre;