-- charla.sales definition
DROP TABLE IF EXISTS charla.sales;
CREATE TABLE charla.sales (
	id serial4 NOT NULL,
	active bool NOT NULL DEFAULT true,
	done bool NULL DEFAULT false,
	sale_id int4 NULL,
	qty int4 NOT NULL,
	price float8 NOT NULL,
	description varchar(255) NULL,
	created_at timestamptz NOT NULL,
	updated_at timestamptz NOT NULL,
	book_id int4 NULL,
	client_id int4 NOT NULL,
	CONSTRAINT sales_pkey PRIMARY KEY (id)
);
-- charla.sales foreign keys
ALTER TABLE charla.sales ADD CONSTRAINT sales_book_id_fkey FOREIGN KEY (book_id) REFERENCES charla.books(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE charla.sales ADD CONSTRAINT sales_client_id_fkey FOREIGN KEY (client_id) REFERENCES charla.clients(id) ON UPDATE CASCADE;
ALTER TABLE charla.sales ADD CONSTRAINT sales_sale_id_fkey FOREIGN KEY (sale_id) REFERENCES charla.sales(id) ON DELETE SET NULL ON UPDATE CASCADE;