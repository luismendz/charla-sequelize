const sequelize = require('./db/');
const Book = require('./db/models/book');
const Author = require('./db/models/author');
const Classification = require('./db/models/classification');
const { Sequelize, Op } = require('sequelize');
const { QueryTypes } = require('sequelize');
(async () => {
    // Generar las querys que se desen probar en esta sección.
    const users = await sequelize.query(`
        SELECT name, lastname
        FROM "charla"."clients" "cl"
        WHERE "cl"."name" like :name AND "cl"."age" >= :age `,
        {
            type: QueryTypes.SELECT,
            replacements: {
                age: 18,
                name: 'A%'
            }
         }
    );
    console.log(
        users
        /* (await Author.findAll({
        limit: 2,
        include: {model: Book},
        order: [
            ['name', 'DESC'],
            [Book, 'year', 'DESC'],
            ['books', 'title', 'ASC']
        ]
    })).map(a=>({...a.get({plain: true}), books: a.get('books').map(b=> `${b.year} - ${b.title}`)})) */ 
    /* await Book.findAll({
        where: {year: {[Op.gte]: 2000}},
        attributes: ['id', 'title']
      }) */
       // (await Classification.findAll({attributes:['id', 'name']})).map(a=>({...a.get({plain: true})}))
    )
})()