# Charla Sequelize
Este repositorio fue creado con razones educativas para probar y aprender el uso de sequelize como ORM. ejecutando pruebas, y visualizando los resultados.

## Composición.
En inicio es un proyecto en latex que genera un PDF con las pruebas, y comentarios útiles para formar una especie de curso guiado a través de los usos mas recurrentes de Sequelize.

En la carpeta src se puede encontrar un proyecto Node.js que se centra en la automatización del llenado de la base de datos para pruebas y los modelos que se estarán usando.

## Siguiente paso
Abrir el PDF charla.pdf en el cual estarán las instrucciones para el seguir la charla.

## Versiones
Las versiones usadas para la creación de este material fueron las siguientes:
- NodeJs: 16.14.2
- Sequelize: 6.32.0
- pg: 8.11.0